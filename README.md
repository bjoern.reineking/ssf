# ssf

Step selection functions


## Installation

You can install the released version of ssf from [gitlab](https://gitlab.irstea.fr) with:

``` r
library(devtools)
devtools::install_git('https://gitlab.irstea.fr/bjoern.reineking/ssf.git')
```